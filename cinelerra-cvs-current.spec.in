# This is not relocatable
%define prefix /usr

# macros
%define with_OPENGL 0

%{?_with_opengl: %{expand: %%global with_OPENGL 1}}

Summary: Cinelerra CVS
Name: cinelerra
#Version: 1.2.0.cvs.cobra
%if %{with_OPENGL}
Version: @VERSION@.CV.svn.%{build_nick}.opengl
%else
Version: @VERSION@.CV.svn.%{build_nick}
%endif
Release: %(date '+%Y%m%d')
License: GPL
Group: X11
URL: http://cvs.cinelerra.org
#Source0: %{name}-%{version}.tar.gz
#Source0: %{name}-1.2.0.tar.gz
Source0: %{name}-@VERSION@.tar.gz
#BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
#BuildRoot: %{_tmppath}/%{name}-1.2.0-%{release}-buildroot
BuildRoot: %{_tmppath}/%{name}-@VERSION@-%{release}-buildroot
Requires: mjpegtools >= 1.6.3
Provides: cinelerra

%description
%if %{with_OPENGL}
Cinelerra unofficial cvs package
build options: --with opengl
%else
Cinelerra unofficial cvs package
without OpenGL support
build options: 
%endif

%prep
# %setup -q
# %setup -n cinelerra-1.2.0
%setup -n %{name}-@VERSION@

%build
%if %{with_OPENGL}
./configure --prefix=%{prefix} --enable-freetype2 --enable-opengl
%else
./configure --prefix=%{prefix} --enable-freetype2
%endif
#./configure
%{__make} %{?_smp_mflags} %{?mflags}

%install
rm -rf $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT%{prefix} install
# rename the mpeg3 utils so they can be installed alongside SuSE native versions
( cd $RPM_BUILD_ROOT%{prefix}/bin
  mv mpeg3toc mpeg3toc.hv
  mv mpeg3cat mpeg3cat.hv
  mv mpeg3dump mpeg3dump.hv
#  cd $RPM_BUILD_ROOT%{prefix}/lib
  ln -s /usr/bin/mpeg2enc $RPM_BUILD_ROOT%{prefix}/lib/cinelerra/mpeg2enc.plugin
)

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
#%{prefix}/bin/*
%{prefix}/bin/cinelerra

# missing in the 2.0 build?
#%{prefix}/bin/mplexhi
%{prefix}/bin/mplexlo

#%exclude %{prefix}/bin/mpeg3dump
#%exclude %{prefix}/bin/mpeg3toc
#%exclude %{prefix}/bin/mpeg3cat
%{prefix}/bin/mpeg3dump.hv
%{prefix}/bin/mpeg3toc.hv
%{prefix}/bin/mpeg3cat.hv
#%{prefix}/lib/*
%{prefix}/share/locale/*
%exclude %{prefix}/include/*

%{prefix}/lib/cinelerra/*.so
%{prefix}/lib/cinelerra/mpeg2enc.plugin
%{prefix}/lib/cinelerra/shapewipe/*

# remove below for no static libs
# %exclude %{prefix}/lib/cinelerra/*.a
%exclude %{prefix}/lib/cinelerra/*.la

%{prefix}/lib/cinelerra/fonts/*

%{prefix}/lib/libguicast*
%{prefix}/lib/libmpeg3hv*
%{prefix}/lib/libquicktimehv*
# missing in 2.0
#%{prefix}/lib/libsndfilehv*
#%{prefix}/lib/pkgconfig*

%{prefix}/share/applications/cinelerra.desktop
%{prefix}/share/pixmaps/cinelerra.xpm


%changelog
* Thu Sep 14 2006 Kevin Brosius <cobra@compuserve.com> - 2.1.0.cvs.cobra-date
- First package with merged Cinelerra 2.1
- svn r888

%changelog
* Thu Jun 1 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- transitions fix, Ogg renderfarm support, po email address fix
- file.h compile fix, fix thread delete race condition
- a bunch of mem management fixes

* Mon May 29 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- reading ogg vorbis fix, Freetype2 2.2.1 compatibility
- mjpeg load fixes (bug139, 148, 259), fix insertion of media files directly
- shorten xml files, motion blur plugin
- camera and projector automation for plugins, waveform not shown fix
- change every clip all media files behavior in saved xml
- work without audio enabled, delete pluginsets fix

* Tue May 02 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- fseek0 fix bug244, bug250, check sound/audio in dv format
- bug249 vorbis setup fix, chromakey fix, check for libfaac
- fix DV audio read sample impl., move icon in Gnome
- bug30 allow transition at end of track, bug243
- fix crash when rendering plugins, fix null plugin usage
- fix load XML crash, bug242 >1024 properties

* Sat Apr 03 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- EDL export, .mov DV fourcc -> dvc, multi transitions with diff lengths fix
- debian build fixes, new configure checks and libavcodec header fixes
- mux fixes for long silence/empty video, ubuntu build fixes
- RGB*_FLOAT color models for mask engine, gcc 4.1.0 fixes/mpeg2enc
- Makefile cleanups, raise_window() commands, BUZ driver channel editor fixes
- format selection for timecode in titler, build date and CV versioning
- fix --disable-firewire, chromakey-hsv plugin, pt_BR translation updates
- deinterlace plugin updates, cache frames even when not static
- new deinterlacer, timefronts - take from alpha
- fix apply-mask-before-plugins for masks, new timefront plugin
- add histogram split checkbox

* Sat Feb 04 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- vertical pos of auto-zoom text, dv audio offset/framing fix
- Italian translation, Spanish update, deinterlace fix
- mpeg encode params fix

* Sat Jan 26 2006 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- raw dv i/o fixes, textual info for labels, " conversion
- Automation text label editable, audio_position adjust for dv1394

* Sat Dec 24 2005 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- fix bug 219 & 220, make text menu hilight white
- alt+left/right navigates trough edit handles
- parameter name fix, single image dnd fixes, remove mwindowmove dup code
- ffmpeg defaults for elem stream (Bug 164), member function fix
- gettextize language changes for more plugins, French update

* Sun Dec 11 2005 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- Fix renderfarm client crash, align cursor on frames default
- right mouse on keyframe - keyframe popup, guicast / gettextize additions
- Brazilian update, French update
- link mpeg2enc.plugin for SUSE, add mjpegtools >= 1.6.3 dep

* Mon Nov 14 2005 Kevin Brosius <cobra@compuserve.com> - 2.0.0.cvs.cobra-date
- Initial build of CV from svn tree.


